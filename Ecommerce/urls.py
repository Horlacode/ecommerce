from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from ecom import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='index'),
    path('login', auth_views.LoginView.as_view(template_name='ecom/login.html'), name='login'),
    path('product/<int:pk>/', views.item_details, name='item-detail'),
    path('add-to-cart/<int:pk>/', views.add_to_cart, name='add-to-cart'),
    path('remove-from-cart/<int:pk>/', views.remove_from_cart, name='remove-from-cart'),
    path('remove-single-from-cart/<int:pk>/', views.remove_single_from_cart, name='remove-single-from-cart'),
    path('cart-summary', views.cart_summary, name='cart-summary'),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

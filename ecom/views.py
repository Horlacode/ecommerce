from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from .models import Order, OrderItem, Item

# Create your views here.

def home(request):
    template_name = 'ecom/home.html'
    order = Order.objects.get(user=request.user, ordered=False)
    items = Item.objects.all().order_by("-title")
    paginator = Paginator(items, 4)
    page = request.GET.get('page')
    try:
        pagelist = paginator.page(page)
    except PageNotAnInteger:
        pagelist = paginator.page(1)
    except EmptyPage:
        pagelist = paginator.page(paginator.num_pages)
    context = {'items': pagelist, 'Order_Summary': order}
    return render(request, template_name, context)

def item_details(request, pk):
    item = get_object_or_404(Item, pk=pk)
    order = Order.objects.get(user=request.user, ordered=False)
    template_name = 'ecom/item_details.html',
    context = {'item': item, 'Order_Summary': order}
    return render(request, template_name, context)

@login_required
def add_to_cart(request, pk):
    item = get_object_or_404(Item, pk=pk)
    order_item, created = OrderItem.objects.get_or_create(item=item, user=request.user, ordered=False)
    # Get the present user...
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        # if the instance exists....grab the user
        order = order_qs[0]
        #  T0 check if the order_item is in the OrderCharter
        if order.items.filter(item__pk=item.pk).exists():
            order_item.quantity += 1
            order_item.save()
            messages.info(request, f'This item {order_item.item.title} quantity is updated')
            return redirect('cart-summary')
        else:
            order.items.add(order_item)
            messages.success(request, f'This item has been added to your Shopping Cart')
            return redirect('cart-summary')
    else:
        ordered_date = timezone.now()
        order = Order.objects.create(user=request.user, ordered_date=ordered_date)
        order.items.add(order_item)
        messages.success(request, f'This item has been added to your Shopping Cart')
        return redirect('cart-summary')



@login_required
def remove_from_cart(request, pk):
    item = get_object_or_404(Item, pk=pk)
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order = order_qs[0]  # grab user instance of the order
        # the check if the order item is in the order..
        if order.items.filter(item__pk=item.pk).exists():
            order_item = OrderItem.objects.filter(item=item, user=request.user, ordered=False)[0]
            # first remove from order... then completely deletes it..
            order.items.remove(order_item)
            order_item.delete()
            messages.warning(request, f'Item {item.title} is removed')
            return redirect('cart-summary')
        else:
            messages.warning(request, f'Invalid request, item is not present in your cart')
            return redirect('/')
    else:
        messages.warning(request, f'Sorry You do not posses an active order')
        return redirect('/')



@login_required
def remove_single_from_cart(request, pk):
    item = get_object_or_404(Item, pk=pk)
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order = order_qs[0]  # grab the instance of the order
        # check if the order is in the shopping cart
        if order.items.filter(item__pk=item.pk).exists():
            # then grap the particular ordered_item instance
            order_item = OrderItem.objects.filter(user=request.user, item=item, ordered=False)[0]
            if order_item.quantity > 1:
                order_item.quantity -= 1
                order_item.save()
                messages.info(request, f'This item {item.title} quantity is updated')
            else:
                order.items.remove(order_item)
                order_item.delete()
                messages.warning(request, f'This item {item.title} is removed from your cart')
            return redirect('cart-summary')
        else:
            messages.warning(request, f'This item does not exist in your cart! Add now')
            return render(request, 'ecom/item_details.html')
    else:
        messages.warning(request, f'You not have an active order ')
        return redirect('/')


@login_required
def cart_summary(request):
    template_name = 'ecom/cart_summary.html'
    try:
        order = Order.objects.get(user=request.user, ordered=False)
        context = {'Order_Summary': order}
        return render(request, template_name, context)
    except ObjectDoesNotExist:
        messages.warning(request, f'Sorry You do not have an active Order')
        return redirect("cart_summary")



def checkout_view(request):
    pass




















